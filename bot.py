# -*- coding: utf-8 -*-

import vk_api
import viginer as v, caesar as c
from vk_api.longpoll import VkLongPoll, VkEventType

vk = vk_api.VkApi(token='1de45b5216b95c434e51b4d22474412087c196d6acf2553b449c81acb47574f4ba7cec66a151d0034f41b')


STEP_MAIN = 0
STEP_TYPE = 1
STEP_CIPHER_TYPE = 2
STEP_CIPHER_KEY = 3
STEP_CIPHER_TEXT = 4

class user:
    uid = 0
    step = 0
    atype = 0 # 0 - caesar, 1 - viginer
    crypt = False # True = crypt, False = decrypt
    key = ''
    def __init__(self, user_id):
        self.uid = user_id

def send_msg(user_id, msg_txt):
    vk.method('messages.send', {'user_id':user_id, 'message':msg_txt})

def main():
    longpoll = VkLongPoll(vk)
    userList = []
	
    for event in longpoll.listen():
        if event.type == VkEventType.MESSAGE_NEW:
            if event.to_me:
                idx = -1

                for i in range(0, len(userList)):
                    if userList[i].uid == event.user_id:
                        idx = i
                        break
                    
                if idx == -1:
                    idx = len(userList)
                    userList.append(user(event.user_id))

                if userList[idx].step == STEP_MAIN:
                    send_msg(event.user_id, 'Что надобно, товарищ? :\n1 - шифр Цезаря;\n2 - шифр Вижинера')
                    userList[idx].step = STEP_TYPE

                elif userList[idx].step == STEP_TYPE:
                    cipherid = 0
                    if event.text.isnumeric():
                        cipherid = int(event.text)
                    if cipherid in range(1,3):
                        userList[idx].atype = cipherid
                        ans = ''
                        if cipherid == 1: ans = 'ОК, будем работать с шифром Цезаря.'
                        else: ans = 'ОК, будем работать с шифром Вижинера.'
                        ans = ans + '\nЧто делать-то?\n1 - шифрование;\n2 - дешифрование'
                        userList[idx].step = STEP_CIPHER_TYPE
                        send_msg(event.user_id, ans)
                    else:
                        send_msg(event.user_id, 'Браток, ты ошибся!. Повтори попытку:\n1 - шифр Цезаря;\n2 - шифр Вижинера')

                elif userList[idx].step == STEP_CIPHER_TYPE:
                    actionid = 0
                    if event.text.isnumeric():
                        actionid = int(event.text)
                    if actionid in range(1,3):
                        ans = ''
                        if actionid == 1:
                            userList[idx].crypt = True
                            ans ='Ну-с шифруем.'
                        else:
                            userList[idx].crypt = False
                            ans = 'Ох... попробуем дешифровать твои каракули'
                        if userList[idx].atype == 1:
                            userList[idx].step = STEP_CIPHER_TEXT
                            ans = ans + '\nТак, жду текст:'
                        else:
                            userList[idx].step = STEP_CIPHER_KEY
                            ans = ans + '\nКлюч, мне нужен ключ!!:'
                        send_msg(event.user_id, ans)
                    else:
                        send_msg(event.user_id, 'Браток, ты ошибся!. Повтори попытку:\n1 - шифрование;\n2 - дешифрование')

                elif userList[idx].step == STEP_CIPHER_KEY:
                    if len(event.text) > 0:
                        userList[idx].step = STEP_CIPHER_TEXT
                        userList[idx].key = event.text
                        send_msg(event.user_id, 'Спасибо, хоть ключ не пожалел. Пиши текст, Пушкин:')
                    else:
                        send_msg(event.user_id, 'ТАААК ерор, мне нужен текст-ключ:')

                elif userList[idx].step == STEP_CIPHER_TEXT:
                    if len(event.text) > 0:
                        ans = ''
                        if userList[idx].atype == 1:
                            if userList[idx].crypt == True: ans = c.encrypt_caesar(event.text)
                            else: ans = c.decrypt_caesar(event.text)
                        else:
                            if userList[idx].crypt == True: ans = v.encrypt_viginere(event.text, userList[idx].key)
                            else: ans = v.decrypt_viginere(event.text, userList[idx].key)    
                        send_msg(event.user_id, 'Фух, это было трудно!:\n' + ans)
                        del userList[idx]
                    else:
                        send_msg(event.user_id, 'Мимо, двай еще попробуем, только постарайся на это раз!:')
						
if __name__ == '__main__':
    main()
